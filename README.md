# Начальная настройка #

* Запустите команду:
```
php install.php
```
* Сконфигурируйте подключение к БД в файле `protected/configs/db.php`

* После настройки подключения к БД запустите миграции:

```
vendor\bin\doctrine-migrations migrate
```