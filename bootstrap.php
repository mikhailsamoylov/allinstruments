<?php

$config = include 'configs/main.php';
foreach ($config['autoload'] as $path) {
    foreach (scandir($path) as $file) {
        if ($file == '.' || $file == '..') {
            continue;
        }
        require_once $path . DIRECTORY_SEPARATOR . $file;
    }
}
