<?php


abstract class BaseRepository
{
    private $repository;

    public function __construct()
    {
        $em = DB::getInstance();
        $this->repository = $em->getRepository($this->modelName());
    }

    public function getRepository()
    {
        return $this->repository;
    }

    abstract protected function modelName(): string;
}
