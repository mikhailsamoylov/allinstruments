<?php

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

require_once __DIR__ . "/../vendor/autoload.php";

class DB
{
    /**
     * @var EntityManager
     */
    private static $instance;

    private function __construct() {}
    private function __wakeup() {}

    public static function getInstance(): EntityManager
    {
        if (is_null(static::$instance)) {
            static::$instance = static::getEntity();
        }
        return static::$instance;
    }

    private static function getEntity(): EntityManager
    {
        $isDevMode = true;
        $config = Setup::createAnnotationMetadataConfiguration([__DIR__ . '/../models'], $isDevMode);
        $conn = include __DIR__ . '/../configs/db.php';
        return EntityManager::create($conn, $config);
    }
}
