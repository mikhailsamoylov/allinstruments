<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RestApplication
{
    public function run()
    {
        $request = Request::createFromGlobals();
        $pathParts = explode('/', $request->getPathInfo());
        $controllerClass = ucfirst($pathParts[1]) . 'Controller';
        if (class_exists($controllerClass)) {
            $controller = new $controllerClass($request);
            if ($controller instanceof RestController) {
                $actionId = $request->getMethod();
                $lastPart = implode('.', array_slice($pathParts, 2));
                if ($lastPart) {
                    $actionId = "$actionId.$lastPart";
                }
                $controller->run($actionId);
            }
        } else {
            $response = new Response('Bad request', 400);
            $response->send();
        }
    }
}
