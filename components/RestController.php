<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class RestController
{
    protected $request;
    protected $response;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->response = new Response();
    }

    public function run($actionId)
    {
        $actionName = $this->getActionMethodName($actionId);
        try {
            ob_start();
            if (method_exists($this, $actionName)) {
                $this->runAction($actionName);
            } else {
                $this->badRequest();
            }
        } catch (Exception $e) {
            $this->handleUnprocessedException($e);
        }
    }

    private function getActionMethodName(string $actionId): string
    {
        $actionNameParts = explode('.', $actionId);
        return 'action' . implode('',
                array_map(
                    function ($part) {
                        return ucfirst(strtolower($part));
                    }, $actionNameParts
                )
            );
    }

    private function runAction(string $actionName): void
    {
        $this->$actionName();
        $this->response->setContent(ob_get_clean());
        $this->response->send();
    }

    private function badRequest()
    {
        $this->response->setStatusCode(400);
        $this->response->setContent('Bad request');
        ob_get_clean();
        $this->response->send();
    }

    private function handleUnprocessedException(Exception $e)
    {
        $this->renderResultJson(['error' => $e->getMessage()]);
        $this->response->setStatusCode(500);
    }

    protected function renderResultJson($data)
    {
        $this->response->headers->set('Content-Type', 'application/json');
        echo json_encode($data);
    }

    protected function getRequestJson()
    {
        return json_decode($this->request->getContent());
    }
}
