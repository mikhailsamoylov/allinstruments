<?php

abstract class RestCrudController extends RestController
{
    abstract protected function modelName(): string;
}
