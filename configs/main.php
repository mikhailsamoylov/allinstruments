<?php

return [
    'autoload' => [
        'components',
        'controllers',
        'helpers',
        'models',
        'repositories',
        'services'
    ]
];
