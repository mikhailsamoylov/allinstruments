<?php


class ItemsController extends RestCrudController
{
    protected function modelName(): string
    {
        return 'Item';
    }

    protected function actionGet()
    {
        $itemRepository = new ItemRepository();
        $this->renderResultJson($itemRepository->getRepository()->findAll());
    }

    protected function actionPostGenerate()
    {
        $service = new ItemsService();
        $this->renderResultJson($service->generateItems(20));
    }
}
