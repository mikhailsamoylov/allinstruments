<?php


class OrdersController extends RestCrudController
{
    protected function modelName(): string
    {
        return 'orders';
    }

    public function actionPostCreate()
    {
        $body = $this->getRequestJson();
        $orderService = new OrdersService();
        $this->renderResultJson($orderService->createOrder($body));
    }

    public function actionPutPay()
    {
        $body = $this->getRequestJson();
        $orderService = new OrdersService();
        $this->renderResultJson($orderService->payOrder($body));
    }
}
