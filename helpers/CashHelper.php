<?php


class CashHelper
{
    public static function compare(float $a, float $b): int
    {
        $epsilon = 0.00001;
        $diff = abs($a - $b);

        if ($diff < $epsilon) {
            return 0;
        } else {
            return $a <=> $b;
        }
    }
}