<?php


$defaultDbConfigContent = <<<EOL
<?php

return [
    'dbname' => 'ai',
    'user' => 'root',
    'password' => 'root',
    'host' => 'localhost',
    'driver' => 'pdo_mysql',
];
EOL;

$filePath = 'configs/db.php';
if (!file_exists($filePath)) {
    file_put_contents('configs/db.php', $defaultDbConfigContent);
}

system('composer install');
