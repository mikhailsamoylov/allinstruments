<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Type;
use Doctrine\Migrations\AbstractMigration;


final class Version20190803112327 extends AbstractMigration
{
    const TABLE_NAME = 'items';

    public function getDescription() : string
    {
        return 'It creates table of items';
    }

    public function up(Schema $schema) : void
    {
        $table = $schema->createTable(static::TABLE_NAME);
        $table->addColumn('id', Type::BIGINT)->setAutoincrement(true);
        $table->addColumn('name', Type::STRING)->setNotnull(true);
        $table->addColumn('price', Type::DECIMAL)->setUnsigned(true)
            ->setNotnull(false)->setPrecision(15)->setScale(2);
        $table->setPrimaryKey(['id']);
    }

    public function down(Schema $schema) : void
    {
        $schema->dropTable(static::TABLE_NAME);
    }
}
