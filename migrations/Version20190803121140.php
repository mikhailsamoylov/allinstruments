<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Type;
use Doctrine\Migrations\AbstractMigration;


final class Version20190803121140 extends AbstractMigration
{
    const TABLE_NAME = 'order_statuses';

    public function getDescription() : string
    {
        return 'It creates table of order statuses';
    }

    public function up(Schema $schema) : void
    {
        require_once __DIR__ . '\..\bootstrap.php';
        $table = $schema->createTable(static::TABLE_NAME);
        $table->addColumn('id', Type::BIGINT)->setAutoincrement(true);
        $table->addColumn('name', Type::STRING)->setNotnull(true);
        $table->setPrimaryKey(['id']);
        $table->addUniqueIndex(['name']);
    }

    public function down(Schema $schema) : void
    {
        $schema->dropTable(static::TABLE_NAME);
    }
}
