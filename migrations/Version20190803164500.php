<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Type;
use Doctrine\Migrations\AbstractMigration;


final class Version20190803164500 extends AbstractMigration
{
    const TABLE_NAME = 'orders';

    public function getDescription() : string
    {
        return 'It creates table of orders';
    }

    public function up(Schema $schema) : void
    {
        $table = $schema->createTable(static::TABLE_NAME);
        $table->addColumn('id', Type::BIGINT)->setAutoincrement(true);
        $table->addColumn('status_id', Type::BIGINT)->setNotnull(true);
        $table->setPrimaryKey(['id']);
        $table->addForeignKeyConstraint('order_statuses', ['status_id'], ['id'], ['onDelete' => 'CASCADE']);
    }

    public function down(Schema $schema) : void
    {
        $schema->dropTable(static::TABLE_NAME);
    }
}
