<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Type;
use Doctrine\Migrations\AbstractMigration;


final class Version20190803220447 extends AbstractMigration
{
    const TABLE_NAME = 'order_items';

    public function getDescription() : string
    {
        return 'It creates order and item link table';
    }

    public function up(Schema $schema) : void
    {
        $table = $schema->createTable(static::TABLE_NAME);
        $table->addColumn('order_id', Type::BIGINT)->setNotnull(true);
        $table->addColumn('item_id', Type::BIGINT)->setNotnull(true);
        $table->addColumn('quantity', Type::INTEGER)->setNotnull(true);
        $table->setPrimaryKey(['order_id', 'item_id']);
        $table->addForeignKeyConstraint('orders', ['order_id'], ['id'], ['onDelete' => 'CASCADE']);
        $table->addForeignKeyConstraint('items', ['item_id'], ['id'], ['onDelete' => 'CASCADE']);
    }

    public function down(Schema $schema) : void
    {
        $schema->dropTable(static::TABLE_NAME);
    }
}
