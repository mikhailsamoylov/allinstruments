<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20190813194131 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'It adds two predefined order statuses';
    }

    public function up(Schema $schema) : void
    {
        $em = \DB::getInstance();
        foreach ([\OrderStatusRepository::$NEW, \OrderStatusRepository::$PAID] as $statusAlias) {
            $status = new \OrderStatus();
            $status->setName($statusAlias);
            $em->persist($status);
        }
        $em->flush();
    }

    public function down(Schema $schema) : void
    {
        $statusRepository = new \OrderStatusRepository();
        $statusRepository->deleteStatusByAlias(\OrderStatusRepository::$NEW);
        $statusRepository->deleteStatusByAlias(\OrderStatusRepository::$PAID);
    }
}
