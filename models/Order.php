<?php

/**
 * @Entity @Table(name="orders")
 **/
class Order implements JsonSerializable
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;

    /** @Column(name="status_id", type="integer") **/
    protected $statusId;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getStatusId(): int
    {
        return $this->statusId;
    }

    public function setStatusId(int $statusId): void
    {
        $this->statusId = $statusId;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'status_id' => $this->statusId,
        ];
    }
}
