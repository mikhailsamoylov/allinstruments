<?php

/**
 * @Entity @Table(name="order_items")
 **/
class OrderItem implements JsonSerializable
{
    /** @Id @Column(name="order_id", type="integer") **/
    protected $orderId;

    /** @Id @Column(name="item_id", type="integer") **/
    protected $itemId;

    /** @Column(type="integer") **/
    protected $quantity;

    public function getOrderId(): int
    {
        return $this->orderId;
    }

    public function setOrderId(int $orderId): void
    {
        $this->orderId = $orderId;
    }

    public function getItemId()
    {
        return $this->itemId;
    }

    public function setItemId($itemId): void
    {
        $this->itemId = $itemId;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function setQuantity($quantity): void
    {
        $this->quantity = $quantity;
    }

    public function jsonSerialize()
    {
        return [
            'order_id' => $this->orderId,
            'item_id' => $this->itemId,
            'quantity' => $this->quantity
        ];
    }
}
