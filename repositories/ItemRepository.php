<?php


class ItemRepository extends BaseRepository
{
    public function findAllByIds(array $ids)
    {
        $em = DB::getInstance();
        $qb = $em->createQueryBuilder();

        $q = $qb->select('i')
            ->from($this->modelName(), 'i')
            ->where($qb->expr()->in('i.id', $ids))
            ->getQuery();

        return $q->getResult();
    }

    protected function modelName(): string
    {
        return 'Item';
    }
}
