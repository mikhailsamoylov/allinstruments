<?php


class OrderItemRepository extends BaseRepository
{
    public function findAllItemsByOrderId(int $id): array
    {
        $em = DB::getInstance();
        $qb = $em->createQueryBuilder();

        $q = $qb->select('oi')
            ->from($this->modelName(), 'oi')
            ->where($qb->expr()->eq('oi.orderId', $id))
            ->getQuery();

        return $q->getResult();
    }

    protected function modelName(): string
    {
        return 'OrderItem';
    }
}
