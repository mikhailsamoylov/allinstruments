<?php


class OrderRepository extends BaseRepository
{
    protected function modelName(): string
    {
        return 'Order';
    }
}
