<?php


class OrderStatusRepository extends BaseRepository
{
    public static $NEW = 'новый';
    public static $PAID = 'оплачено';

    public function findStatusByAlias(string $alias): ?OrderStatus
    {
        $em = DB::getInstance();
        $qb = $em->createQueryBuilder();

        $q = $qb->select('os')
            ->from($this->modelName(), 'os')
            ->where($qb->expr()->eq('os.name', "'$alias'"))
            ->getQuery();

        return $q->getOneOrNullResult();
    }

    public function deleteStatusByAlias(string $alias)
    {
        $em = DB::getInstance();
        $qb = $em->createQueryBuilder();

        $q = $qb->delete($this->modelName(), 'os')
            ->where($qb->expr()->eq('os.name', "'$alias'"))
            ->getQuery();

        $q->execute();
    }

    protected function modelName(): string
    {
        return 'OrderStatus';
    }
}
