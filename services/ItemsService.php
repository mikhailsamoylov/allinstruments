<?php


class ItemsService
{
    public function generateItems(int $count): array
    {
        $em = DB::getInstance();

        $items = [];
        for ($i = 0; $i < $count; $i++) {
            $item = new Item();
            $item->setName($this->generateName());
            $item->setPrice($this->generatePrice());
            $em->persist($item);
            $items[] = $item;
        }
        $em->flush();

        return $items;
    }

    private function generateName(): string
    {
        $chars = 'qwertyuiopasdfghjklzxcvbnm';
        return ucfirst(substr(str_shuffle(str_repeat($chars, rand(1, 10))), 0, rand(5, 10)));
    }

    private function generatePrice(): float
    {
        return round(rand(0, 10000000) / rand(1, 100), 2);
    }
}
