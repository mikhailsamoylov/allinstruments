<?php


class OrdersService
{
    public function createOrder(array $orderInfo): Order
    {
        $newStatus = $this->getNewStatus();
        $order = $this->saveOrderWithStatus($newStatus);
        $this->fillOrderInfo($order, $orderInfo);

        return $order;
    }

    private function getNewStatus(): OrderStatus
    {
        $statusRepository = new OrderStatusRepository();
        return $statusRepository->findStatusByAlias(OrderStatusRepository::$NEW);
    }

    private function saveOrderWithStatus(OrderStatus $status): Order
    {
        $em = DB::getInstance();
        $order = new Order();
        $order->setStatusId($status->getId());
        $em->persist($order);
        $em->flush();

        return $order;
    }

    private function fillOrderInfo(Order $order, array $orderInfo)
    {
        $em = DB::getInstance();

        foreach ($orderInfo as $row) {
            $orderItem = new OrderItem();
            $orderItem->setOrderId($order->getId());
            $orderItem->setItemId($row->item_id);
            $orderItem->setQuantity($row->quantity ?? 1);
            $em->persist($orderItem);
        }

        $em->flush();
    }

    public function payOrder($orderPay): Order
    {
        $orderId = $orderPay->order_id;
        $totalPrice = $this->getTotalPrice($orderId);

        if (CashHelper::compare($orderPay->cash, $totalPrice) == 0) {
            $order = $this->getOrder($orderId);
            $newStatus = $this->getNewStatus();
            if ($order->getStatusId() == $newStatus->getId()) {
                $responseCode = $this->getResponseCode();
                if ($responseCode == 200) {
                    return $this->setOrderAsPaid($order);
                } else {
                    throw new Exception('https://ya.ru did not return status code 200');
                }
            } else {
                throw new Exception('Order is already paid');
            }
        } else {
            throw new Exception("Please, give me $$totalPrice cash");
        }
    }

    private function getTotalPrice(int $orderId): float
    {
        $orderItems = $this->getOrderItems($orderId);
        if (empty($orderItems)) {
            throw new Exception('Specified order has no items');
        }
        $items = $this->getItems($orderItems);
        $orderItemsGroupedByItemId = $this->groupOrderItemsByItemId($orderItems);
        $totalPrice = 0;
        /** @var Item $item */
        foreach ($items as $item) {
            $totalPrice += $item->getPrice() * $orderItemsGroupedByItemId[$item->getId()]->getQuantity();
        }

        return $totalPrice;
    }

    private function getOrderItems(int $orderId): array
    {
        $orderItemRepository = new OrderItemRepository();
        return $orderItemRepository->findAllItemsByOrderId($orderId);
    }

    private function getItems(array $orderItems): array
    {
        $itemIds = array_map(function (OrderItem $item) {
            return $item->getItemId();
        }, $orderItems);

        $itemRepository = new ItemRepository();
        return $itemRepository->findAllByIds($itemIds);
    }

    private function groupOrderItemsByItemId(array $orderItems): array
    {
        $orderItemsGroupedByItemId = [];
        /** @var OrderItem $orderItem */
        foreach ($orderItems as $orderItem) {
            $orderItemsGroupedByItemId[$orderItem->getItemId()] = $orderItem;
        }

        return $orderItemsGroupedByItemId;
    }

    private function getOrder(int $orderId): Order
    {
        $orderRepository = new OrderRepository();
        return $orderRepository->getRepository()->find($orderId);
    }

    private function getResponseCode(): int
    {
        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, 'https://ya.ru');
        curl_setopt($handle,  CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($handle, CURLOPT_SSL_VERIFYHOST,  2);
        curl_setopt($handle, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($handle, CURLOPT_HEADER, 0);
        curl_exec($handle);
        $responseCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
        curl_close($handle);

        return $responseCode;
    }

    private function setOrderAsPaid(Order $order): Order
    {
        $statusRepository = new OrderStatusRepository();
        $paidStatus = $statusRepository->findStatusByAlias(OrderStatusRepository::$PAID);
        $order->setStatusId($paidStatus->getId());
        $em = DB::getInstance();
        $em->persist($order);
        $em->flush();

        return $order;
    }
}
